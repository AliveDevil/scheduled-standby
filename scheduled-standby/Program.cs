﻿using Microsoft.Win32;

namespace scheduled_standby
{
	internal class Program
	{
		private static App app;

		public static void Main()
		{
			app = new App();

			SystemEvents.PowerModeChanged += SystemEvents_PowerModeChanged;

			app.Run();
		}

		private static void SystemEvents_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
		{
			switch (e.Mode)
			{
				case PowerModes.Resume:
					break;

				case PowerModes.StatusChange:
					break;

				case PowerModes.Suspend:
					break;
			}
		}
	}
}
