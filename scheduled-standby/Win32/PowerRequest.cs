﻿using System;
using System.Runtime.InteropServices;

namespace scheduled_standby.Win32
{
	public sealed class PowerRequest : IDisposable
	{
		private readonly CONTEXT context;
		private readonly IntPtr handle;
		private bool disposedValue = false;

		public PowerRequest()
		{
			context.Version = CONTEXT.VERSION;
			context.Flags = CONTEXT.SIMPLE_STRING;
			context.SimpleReasonString = "scheduled-standby";

			handle = NativeMethods.CreateRequest(ref context);

			NativeMethods.SetRequest(handle, PowerRequestType.DisplayRequired);
			NativeMethods.SetRequest(handle, PowerRequestType.SystemRequired);
		}

		~PowerRequest()
		{
			Dispose(false);
		}

		public enum PowerRequestType
		{
			/// <summary>
			/// The display remains on even if there is no user input for an extended period of time.
			/// </summary>
			DisplayRequired = 0,

			/// <summary>
			/// The system continues to run instead of entering sleep after a period of user inactivity.
			/// </summary>
			SystemRequired,

			/// <summary>
			/// The system enters away mode instead of sleep in response to explicit action by the user.
			/// In away mode, the system continues to run but turns off audio and video to give the appearance of sleep.
			/// </summary>
			AwayModeRequired,

			/// <summary>
			/// The calling process continues to run instead of being suspended or terminated by process lifetime management mechanisms.
			/// When and how long the process is allowed to run depends on the operating system and power policy settings.
			/// </summary>
			/// <remarks>
			/// On systems not capable of connected standby, an active PowerRequestExecutionRequired request implies PowerRequestSystemRequired.
			/// </remarks>
			Maximum
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
				}

				NativeMethods.CloseHandle(handle);

				disposedValue = true;
			}
		}

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		private struct CONTEXT
		{
			public const int VERSION = 0;
			public const int SIMPLE_STRING = 1;
			public const int DETAILED_STRING = 2;

			public uint Version;
			public uint Flags;

			[MarshalAs(UnmanagedType.LPWStr)]
			public string SimpleReasonString;

			[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
			public struct DETAILED
			{
				public uint Version;
				public uint Flags;
				public INFORMATION DetailedInformation;

				[StructLayout(LayoutKind.Sequential)]
				public struct INFORMATION
				{
					public IntPtr LocalizedReasonModule;
					public uint LocalizedReasonId;
					public uint ReasonStringCount;

					[MarshalAs(UnmanagedType.LPWStr)]
					public string[] ReasonStrings;
				}
			}
		}

		private static class NativeMethods
		{
			[DllImport("kernel32.dll", EntryPoint = "PowerClearRequest")]
			public static extern bool ClearRequest(IntPtr PowerRequestHandle, PowerRequestType RequestType);

			[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
			public static extern int CloseHandle(IntPtr hObject);

			// Availability Request Functions
			[DllImport("kernel32.dll", EntryPoint = "PowerCreateRequest")]
			public static extern IntPtr CreateRequest(ref CONTEXT Context);

			[DllImport("kernel32.dll", EntryPoint = "PowerSetRequest")]
			public static extern bool SetRequest(IntPtr PowerRequestHandle, PowerRequestType RequestType);
		}
	}
}
